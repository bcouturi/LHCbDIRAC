.. _administrator_guide:

===================
Administrator Guide
===================


This page is the work in progress. See more material here soon !

.. toctree::
   :maxdepth: 2

   Installation/make_release.rst
   Installation/certificate.rst
   Online.rst
   bookkeeping/adminisrate_oracle.rst
   dataDistribution.rst
   flushing.rst
