#!/bin/bash


filename=$(readlink -f  $BASH_SOURCE)
dir=$(dirname $filename)
LBN_LOC=$(cd  $dir;pwd)

export SYS=`echo $CMTCONFIG | awk -F- '{printf "%s-%s\n",$1,$2}'`
export PATH=${LBN_LOC}/scripts:${LBN_LOC}/${SYS}/bin:${LBN_LOC}/${SYS}/sbin:$PATH
export LD_LIBRARY_PATH=$LBN_LOC/${SYS}/lib:${LD_LIBRARY_PATH}
export PYTHONPATH=${LBN_LOC}:${LBN_LOC}/${SYS}/lib/python2.7/site-packages/:${PYTHONPATH}


