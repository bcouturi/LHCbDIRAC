.PHONY: test scripts

#clean:
#	rm -rf *.out *.xml htmlcov

S=*System

BUNDLE_VERSION=v13r0
EXTERNAL_VERSION=v6r6p3
DIST_TOOLS=dist-tools
PYTHON_VERSION=2.7.9.p1
PYTHON_VERSION_TWO=2.7
PYTHON_VERSION_TWODIGIT=27

OS = $(word 2,$(subst -, ,$(CMTCONFIG)))

ifeq ($(OS),slc6)
  DIRACPLAT=Linux_x86_64_glibc-2.12
endif
ifeq ($(OS),centos7)
  DIRACPLAT=Linux_x86_64_glibc-2.17
endif

all: scripts bundle_$(BUNDLE_VERSION)-$(DIRACPLAT).ts

bundle_$(BUNDLE_VERSION)-$(DIRACPLAT).ts: PREFIX= http://lhcbproject.web.cern.ch/lhcbproject/dist/DIRAC3
bundle_$(BUNDLE_VERSION)-$(DIRACPLAT).ts:
	curl -C - --silent $(PREFIX)/lcgBundles/DIRAC-lcg-$(BUNDLE_VERSION)-$(DIRACPLAT)-python$(PYTHON_VERSION_TWODIGIT).tar.gz | tar zxv
	curl -C - --silent $(PREFIX)/installSource/Externals-server-$(EXTERNAL_VERSION)-$(DIRACPLAT)-python$(PYTHON_VERSION_TWODIGIT).tar.gz | tar zxv 
	ln -s $(DIRACPLAT) $(LCG_hostos)
	touch bundle_$(BUNDLE_VERSION)-$(DIRACPLAT).ts

test: 
	py.test $S --cov=$S

docs: 
	cd docs && make html && cd ..
	
scripts:
	$(DIST_TOOLS)/gen_scripts.py

clean:
	$(RM) $(XENV) $(XENV)c $(MANIFEST)
	
purge: clean
	$(RM) -r InstallArea/$(CMTCONFIG)
	$(RM) -r $(DIRACPLAT) $(LCG_hostos)
	$(RM) -r scripts

# fake targets to respect the interface of the Gaudi wrapper to CMake
configure:
install:
install/fast:
unsafe-install:
post-install:
